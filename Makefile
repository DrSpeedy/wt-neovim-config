NAME=wt-neovim-config
VERSION=1.2.2

PKG_NAME=$(NAME)
PKG_ARCH=all

SYSCONF=/etc
PREFIX?=/usr/share
DOC_DIR=$(PREFIX)/doc/$(PKG_NAME)

BLD_DIR=$(PKG_NAME)_$(VERSION)_$(PKG_ARCH)
PKG=$(BLD_DIR).deb

PUBKEY_ID=EE8BEF82958D1FF33F3B375EBBBB9FB338982697

GIT_URL=https://github.com/DrSpeedy/nvimrc.git
GIT_DIR=nvimrc

pkg: build
	dpkg --build ${BLD_DIR}

build:
	git clone --recurse-submodules -j8 $(GIT_URL)
	mkdir -p $(BLD_DIR)/etc/xdg
	mkdir -p $(BLD_DIR)/$(DOC_DIR)
	mv $(GIT_DIR) $(BLD_DIR)/etc/xdg/nvim
	cp oceanic-next-alphabg.patch $(BLD_DIR)/etc/xdg/nvim
	mv $(BLD_DIR)/etc/xdg/nvim/screenshot.png $(BLD_DIR)/$(DOC_DIR)
	mv $(BLD_DIR)/etc/xdg/nvim/README.md $(BLD_DIR)/$(DOC_DIR)
	cp -r DEBIAN $(BLD_DIR)
	cp LICENSE $(BLD_DIR)/$(DOC_DIR)

sign: pkg
	dpkg-sig -k $(PUBKEY_ID) --sign wiltech $(PKG)
	dpkg-sig -c $(PKG)

clean:
	rm -rf $(PKG) $(BLD_DIR) $(GIT_DIR)

all: pkg sign

tag:
	git tag $(VERSION)
	git push --tags

release: pkg sign tag

install:
	apt install ./$(PKG)

uninstall:
	apt remove --purge $(PKG_NAME)

.PHONY: build sign clean test tag release install uninstall all
